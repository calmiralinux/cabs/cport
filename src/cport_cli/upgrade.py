#!/usr/bin/python3
#
# 'upgrade.py'
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import cport_cli.install as ci
import cport_cli.remove as cr
import libcport.core as cc
import libcport.exceptions as ce
import toml
from libcport.datatypes import port


def check(name: port) -> bool:
    # TODO: добавить проверку соответствия версии установленного
    # порта и порта в СП

    db = cc.CDatabase(name)
    code = db.check()
    db.close()

    return code


def upgrade_soft(name: port) -> int:
    db = cc.CDatabase(name)
    if not db.check():
        cc.Msg.err(f"Error: port '{name.name}' not in database!")
        return 1

    db.remove()
    db.close()

    run = ci.install(name, False, False)

    if run != 0:
        return run
    return 0


def upgrade_hard(name: port) -> int:
    if not check(name):
        cc.Msg.err(f"Error: port '{name.name}' not in database!")
        return 1

    run = cr.remove(name)

    if run != 0:
        return run

    run = ci.install(name, False, False)

    if run != 0:
        return run
    return 0


def main(args: list[str]) -> int:
    for arg in args:
        try:
            arg = port(arg)
            data = toml.load(f"{arg.path}/port.toml")
            upgrade_mode = data['package']['upgrade_mode']

            if upgrade_mode == "soft":
                run = upgrade_soft(arg)
            elif upgrade_mode == "hard":
                run = upgrade_hard(arg)
            else:
                cc.Msg.err(f"Port '{arg.name}' is broken:" \
                        f" 'package.upgrade_mode' is invalid: '{upgrade_mode}'")
                cc.Msg.err("Valid names: soft & hard")
                return 1

            if run != 0:
                return run
        except ce.PortNotFoundError:
            cc.Msg.err(f"Error: port '{arg}' not found!")
            return 1
        except toml.decoder.TomlDecodeError:
            cc.Msg.err("Error decoding the configuration file of the port" \
                    f" '{arg}'!")
            return 1
    return 0
