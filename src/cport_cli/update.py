#!/usr/bin/python3
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from shutil import get_terminal_size
from urllib.error import URLError

import toml

import libcport.core as cc
from libcport.constants import CACHE_UNPACKED_DIR
from libcport.constants import CALMIRA_RELEASE
from libcport.datatypes import port
from libcport.update import Update


def check() -> int:
    """
    Принцип работы `check()`:

    - Скачивание архива системы портов из репозитория;
    - Распаковка архива системы портов в кеш (`CACHE_UNPACKED_DIR`);
    - Получение номера версии Calmira GNU/Linux-libre;
    - Сравнение версии дистрибутива Calmira и релиза, указанного в метаданных СП:
        - если версия установленного дистрибутива не содержится в метаданных СП, прервать обновление;
    - Получение списка изменений:
        - все изменения;
        - добавленные порты;
        - удалённые порты;
        - обновлённые порты.
    - Формирование списка изменений в том случае, если хотя бы один из списков >= 1:
        - вывод всего, что добавлено;
        - вывод всего, что удалено;
        - вывод обновлений (указание старой версии - которая в установленной СП, указаное новой версии из новой СП);
    - Обновление системы портов:
        - Удаление существующей СП в `PORTS_DIR`;
        - Копирование скачанной распакованной СП в `PORTS_DIR`;
    """

    u = Update(CACHE_UNPACKED_DIR)
    try:
        cc.Msg.msg("Downloading the port system...")
        u.get()
    except URLError:
        cc.Msg.err("Temporary failure in name resolution!")
        return 1

    cc.Msg.msg(f"Unpacking ports in '{CACHE_UNPACKED_DIR}'...")
    u.unpack()

    try:
        rel = u.check_release()
    except KeyError:
        cc.Msg.err(f"File '{CALMIRA_RELEASE}' is not valid file!")
        return 1

    if not rel:
        ports_rel_metadata = u._get_metadata(mode=1)['system']['release']
        sys_rel = cc.get_calm_release()

        ports_rel = ""
        for i in ports_rel_metadata:
            ports_rel += str(i) + " "

        print(f"The port system is for version: {ports_rel}")
        print(f"Version system installed: {sys_rel}")
        err_msg = "The ports system is not compatible with the current " \
                  "release of Calmira GNU/Linux(-libre)!"
        cc.Msg.err(err_msg)

        return 1

    cc.Msg.msg("Generating list of changes...")

    try:
        added = u.get_added_ports()
        removed = u.get_removed_ports()
        updates = u.get_updated_ports()
        other = u.get_other_ports()
    except KeyError:
        cc.Msg.err(
            "Error parsing the metadata of the installed or updated ports system"
        )
        return 1

    width = get_terminal_size()[0]

    if width < 80:
        cc.Msg.warn("The terminal is very small! Required width: 80 or more chars.")
        return 1

    if len(added) > 0 or len(removed) > 0 or len(updates) > 0 or len(other) > 0:
        pass
    else:
        cc.Msg.err("No updates.")
        return -1

    if len(added) > 0 or len(removed) > 0:
        print("\n\033[1mAdditions/deletions:\033[0m")
        print("| {0:58} | {1:15} |".format("name", "status"))
        print("|{0}|{1}|".format(60 * '-', 17 * '-'))

        if len(added) > 0:
            for i in added:
                print("| {0:58} | {1:15} |".format(str(i), "added"))
        if len(removed) > 0:
            for i in removed:
                print("| {0:58} | {1:15} |".format(str(i), "removed"))

    if len(updates) > 0:
        print("\n\033[1mUpgrades/downgrades:\033[0m")
        print(
            "| {0:40} | {1:15} | {2:15} |".format(
                "name", "old version", "new version"
            )
        )
        print("|{0}|{1}|{1}|".format(42 * '-', 17 * '-'))

        for change in updates:
            name = change['name']
            vers = change['version']

            db = cc.CDatabase(port(name))
            if db.check():
                name = f"[i] {name}"

            print(
                "| {0:40} | {1:15} | {2:15} |".format(
                    name, vers['old'], vers['new']
                )
            )

    if len(other) > 0:
        print("\n\033[1mOther changes:\033[0m")
        print("| {0:58} | {1:15} |".format("name", "version"))
        print("|{0}|{1}|".format(60 * '-', 17 * '-'))

        for name in other:
            port_name = port(name)
            data = toml.load(f"{port_name.path}/port.toml")
            vers = data['package']['version']
            print("| {0:58} | {1:15} |".format(name, vers))

    return 0
