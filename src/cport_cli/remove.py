#!/usr/bin/python3
#
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import libcport.core as cc
import libcport.exceptions as ce
import libcport.remove as cr
import libcport.datatypes as cd


def remove(port: cd.port, ignore_db: bool = False, confirm: bool = False, skip_check: bool = False) -> int:
    db = cc.CDatabase(port)

    if not db.check() and not ignore_db:
        cc.Msg.err(f"Port '{port.name}' doesn't exist in the database!")
        return 1

    rm = cr.CRemove(port)

    if rm.check_priority():
        cc.Msg.err(f"Port '{port.name}' has system priority")
        return 1

    if not confirm and not cc.Msg.dialog(default_no=True):
        return 1

    cc.Msg.msg(f"Remove the '{port.name}' files from the system...")

    try:
        prerem = rm.run_preremove()

        if prerem != 0:
            cc.Msg.err(f"Execution of pre-remove script failed (code: {prerem}")
            return prerem

        rm.remove()

        postrem = rm.run_postremove()

        if postrem != 0:
            cc.Msg.err(
                f"Execution of post-remove script failed (code: {postrem}"
            )
            return postrem

    except FileNotFoundError as e:
        cc.Msg.err(f"File not found: {e}")
        return 1
    except ce.PortFilesBrokenError:
        cc.Msg.err(f"Port '{port.name}' is broken!")
        return 1

    if not skip_check:
        cc.Msg.msg("System check...")
        check = cc.check(port)

        if check:
            cc.Msg.err("System check error!")
            # TODO: переделать поведение cc.check().
            # Пусть она возвращает dict со списком
            # присутствующих и отсутствующих файлов

            return 1

        code = 0
        if not ignore_db:
            cc.Msg.msg(f"Remove port '{port.name}' from database...", endl=" ")

            db.remove()
            if not db.check():
                print("OK")
            else:
                print("ERROR!")
                code = 1

        db.close()

        return code
    return 0
