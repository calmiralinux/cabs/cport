#!/usr/bin/python3
#
# core.py - core objects for cport package manager
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
'core.py' is the main cport source code that is the core of this package
manager. Contains basic functionality for getting information about the Calmira
GNU/Linux-libre distribution (required to check the port for compatibility with
the current release of Calmira).

Tasks solved by this module:

* Obtaining information about the installed system;
* Checking for files belonging to a specific port;
* More high-level interaction with the database;
"""

import libcport.datatypes as cd
import libcport.exceptions as ce
import libcport.constants as cc

import os
import sys
import toml
import time
import shutil
import sqlite3


def dbg_msg(msg: str, endl="\n") -> None:
    # CPORT_DBG='yes' or CPORT_DBG='no' or CPORT_DBG is not set
    cport_dbg: str | None = os.environ.get('CPORT_DBG')

    if cport_dbg is not None and cport_dbg == "yes":
        print(f"[DEBUG] {msg}", end=endl)

        with open('.cport.debug.log', 'a') as f:
            msg = fr"{msg}"
            f.write(f"{msg}\n")


def ckuser() -> bool:
    """Checks if cport is running as root.
    If running, returns True, otherwise
    returns False."""

    return os.geteuid() == 0


def get_calm_release() -> str:
    data = toml.load(cc.CALMIRA_RELEASE)
    return data['system']['version']


def check(name: cd.port) -> int | None:
    """Checks if a port is installed to the system or not.

    Statuses:
        - 0 - 100% files;
        - 1 - 75-99% files;
        - 2 - 50-74% files;
        - 3 - 5-49% files;
    """

    files_list = f"{name.path}/files.list"
    fcount = len(files_list)
    f_install_count = 0

    with open(files_list) as f:
        dbg_msg(f"[check] open '{files_list}'...")
        files = f.read().splitlines()

    for file in files:
        if not os.path.exists(file):
            dbg_msg(f"[files checking...] File '{file}':\033[31m not found\033[0m")
        else:
            f_install_count += 1

    if f_install_count == fcount:
        return 0
    elif f_install_count >= (fcount / 0.75):
        return 1
    elif f_install_count >= (fcount / 0.5):
        return 2
    elif f_install_count >= (fcount / 0.05):
        return 3


def p_lint(name: str) -> bool:
    """Ports linter. Checks that the `port.toml` and
    `files.list` files of the port are correct"""

    try:
        path = cd.port(name).path
    except ce.PortBrokenError:
        return False
    except ce.PortNotFoundError:
        return False

    ret_code = True

    # Checking the "port.toml" file
    try:
        dbg_msg("[p_lint] checking the 'port.toml' file")

        data = toml.load(f"{path}/port.toml")

        # Проверка наличия секций "package" и "port"
        if data.get('package') is None or data.get('port') is None:
            raise ce.FilePortTomlError

        # Проверка наличия необходимых параметров в секции "package"
        # Параметры в секции "port" являются необязательными
        for param in cc.PORT_TOML_PARAMS["package"]:
            if data["package"].get(param) is None:
                raise ce.FilePortTomlError

    except toml.TomlDecodeError:
        raise ce.FilePortTomlError
    except FileNotFoundError:
        raise ce.FilePortTomlError

    # Checking the "files.list" file
    try:
        dbg_msg("Checking the 'files.list' file")
        dbg_msg(f"{path}/files.list")

        with open(f"{path}/files.list") as f:
            flist = f.read().splitlines()

        if len(flist) < 1:
            raise ce.FileFilesListError

    except FileNotFoundError:
        raise ce.FileFilesListError

    return ret_code


def get_files_in_cache() -> int:
    dirs = (cc.CACHE_UNPACKED_DIR, cc.CACHE_DOWNLOADED_DIR)
    files = 0

    for _dir in dirs:
        dbg_msg(f"[get_files_in_cache] work directory: '{_dir}'")

        fls = os.listdir(_dir)
        files += len(fls)
        for _ in os.walk(_dir):
            files += 1

    dbg_msg(f"Files count: {files}")
    return files


def wipe_cache() -> None:
    """This method wipes all data in the cache"""

    dirs = (cc.CACHE_UNPACKED_DIR, cc.CACHE_DOWNLOADED_DIR)

    for _dir in dirs:
        dbg_msg(f"Clearing the '{_dir}' directory...")
        fls = os.listdir(_dir)
        for file in fls:
            file = f"{_dir}/{file}"
            dbg_msg(f"Detect the '{file}' file")
            if os.path.isfile(file):
                os.remove(file)
                dbg_msg("This is file. Removed")
            elif os.path.isdir(file):
                shutil.rmtree(file)
                dbg_msg("This is a dir. Removed")


class CDatabase(object):

    def __init__(self, name: cd.port, dbfile: str = cc.DATABASE_FILE):
        self.name = name

        dbg_msg(f"Работа с БД по порту '{name.name}'")

        with open(f"{name.path}/port.toml") as f:
            self.port_data = toml.load(f)

        self.con = sqlite3.connect(dbfile)
        self.cur = self.con.cursor()

    def close(self) -> None:
        self.cur.close()

    def create_initial_db(self) -> None:
        queries = """CREATE TABLE ports_in_fs (
            name TEXT, version TEXT, description TEXT, maintainer TEXT,
            releases TEXT, priority TEXT, usage REAL, upgrade_mode TEXT,
            build_time REAL
        );
        CREATE TABLE installed_ports (
            name TEXT, version TEXT, description TEXT, maintainer TEXT,
            releases TEXT, priority TEXT, usage REAL, upgrade_mode TEXT,
            build_time REAL, status INT, build_date REAL
        );"""

        self.cur.executescript(queries)

    def add(self, status: int = 0, table: str = "installed_ports") -> None:
        """Statuses:
        - 0 - 100% files;
        - 1 - 75-99% files;
        - 2 - 50-74% files;
        - 3 - 5-49% files;
        """

        conf = self.port_data
        params = (
            'version', 'description',
            'maintainer', 'releases', 'priority',
            'usage', 'upgrade_mode', 'build_time'
        )

        data = [self.name.name]

        for prm in params:
            tp_str = ""
            # The next step is to check the received data. In the event that the
            # data type 'list' is received, then it is converted to a 'str'
            if type(conf['package'].get(prm)) == list:
                for i in conf['package'][prm]:
                    tp_str += str(i) + " "
            else:
                tp_str = conf['package'].get(prm)
            data.append(tp_str)

        data.append(status)  # Status

        if table == "installed_ports":
            data.append(time.time())
            vlist = "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        else:
            vlist = "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

        db_query = f"INSERT INTO {table} VALUES {vlist}"
        self.cur.execute(db_query, data)
        self.con.commit()

    def remove(self, table: str = "installed_ports") -> None:
        name = self.name.name
        self.cur.execute(f"DELETE FROM {table} WHERE name = '{name}'")
        self.con.commit()

    def get_one(self, table: str = "installed_ports"):
        name = self.name.name
        dt = self.cur.execute(f"SELECT * FROM {table} WHERE name = ?", (name,))

        return dt.fetchone()

    def get_all(self, table: str = "installed_ports"):
        """Warning: this method is too slow!"""

        name = self.name.name
        dt = self.cur.execute(f"SELECT * FROM {table} WHERE NAME = ?", (name,))

        return dt.fetchall()

    def check(self, table: str = "installed_ports") -> bool:
        query = f"SELECT * FROM {table} WHERE NAME = ?"
        dt = self.cur.execute(query, (self.name.name,))

        return dt.fetchone() is not None

    def execute(self, query: str, params: tuple, commit: bool = False):
        data = self.cur.execute(query, params)

        if commit:
            self.con.commit()

        return data.fetchall()

    def gen_all_ports(self) -> None:
        metadata = toml.load(cc.METADATA_FILE)
        ports = metadata['port_sys']['ports']

        self.cur.execute("DELETE FROM ports_in_fs")

        for port in ports:
            port = cd.port(port)
            port_conf = toml.load(f"{port.path}/port.toml")
            data = [port.name]
            params = (
                'version', 'description', 'maintainer',
                'releases', 'priority', 'usage',
                'upgrade_mode', 'build_time'
            )

            for param in params:
                tp_str = ""
                if type(port_conf['package'].get(param)) == list:
                    # Converting list to str
                    for i in port_conf['package'][param]:
                        tp_str += str(i) + " "
                else:
                    tp_str = port_conf['package'].get(param)
                data.append(tp_str)

            db_query = "INSERT INTO ports_in_fs VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
            self.cur.execute(db_query, data)
            self.con.commit()


class CList(object):

    def __init__(self):
        self.md = toml.load(cc.METADATA_FILE)
        self.ports = self.md['port_sys']['ports']

    def all(self) -> list[str]:
        return self.ports

    def installed(self) -> list[tuple[str, float]]:
        """Returns list of tuples with values:

            (port name, installation date)

        Types:

             (str, float)

        The installation time is returned in UNIX Time format.
        """

        ports_list = []

        for port in self.ports:
            dbg_msg(f"Selected port '{port}'...")

            try:
                db = CDatabase(cd.port(port))

                if db.check():
                    dbg_msg("This port is installed")

                    data = db.get_one()
                    ports_list.append((data[0], data[-1]))

                db.close()
            except ce.PortNotFoundError:
                dbg_msg(f"Port '{port}' not found")
                continue
            except ce.PortBrokenError:
                dbg_msg(f"Port '{port}' is broken")
                continue

        return ports_list

    def not_installed(self) -> list[str]:
        ports_list = []
        for port in self.ports:
            try:
                db = CDatabase(cd.port(port))
                if not db.check():
                    db.close()
                    ports_list.append(port)
                db.close()
            except ce.PortNotFoundError:
                dbg_msg(f"Port '{port}' not found")
                continue
            except ce.PortBrokenError:
                dbg_msg(f"Port '{port}' is broken")
                continue
        return ports_list

    def broken(self) -> list[str]:
        ports_list = []
        for port in self.ports:
            try:
                cd.port(port)
            except ce.PortBrokenError:
                ports_list.append(port)
        return ports_list


def rebuild() -> None:
    cl = CList()
    ports = cl.all()

    for port in ports:
        try:
            port = cd.port(port)
            db = CDatabase(port)
        except ce.PortNotFoundError:
            dbg_msg(f"-- cport kernel -- port '{port}' not found in the filesystem")
            continue

        files_list_file = f"{port.path}/files.list"

        if not os.path.exists(files_list_file):
            dbg_msg(f"File '{files_list_file}' of port '{port.name}' not found")
            continue

        if db.check():
            dbg_msg(f"port '{port.name}' already in DB! Skipping...")
            continue

        with open(files_list_file) as f:
            files = f.read().splitlines()

        files_count = 0

        for file in files:
            if os.path.exists(file):
                files_count += 1

        if files_count == len(files):
            port_status = 0
        elif len(files) / 0.75:
            port_status = 1
        elif files_count >= (len(files) / 0.5):
            port_status = 2
        elif files_count >= (len(files) / 0.05):
            port_status = 3
        else:
            port_status = 4

        if port_status == 4:
            db.close()
            continue

        db.add(status=port_status)
        db.close()


def log_msg(msg: str, status: str = "info") -> None:
    file = cc.LOG_FILE if ckuser() else cc.USER_LOG_FILE

    with open(file, 'a') as f:
        msg = f"[ {time.ctime()} ] [ {status} ] {msg}"
        f.write(msg)


class Msg:

    @staticmethod
    def log_msg(msg: str, status: str = "info") -> None:
        file = cc.LOG_FILE if ckuser() else cc.USER_LOG_FILE

        with open(file, 'a') as f:
            msg = f"[ {time.ctime()} ] [ {status} ] {msg}"
            f.write(msg)

    @staticmethod
    def dialog(default_no: bool = False) -> bool:
        print("\n:: Continue?", end=" ")
        if default_no:
            print("[y/N]", end=" ")
        else:
            print("[Y/n]", end=" ")
        run = input()
        if run == "N" or run == "n":
            return False
        elif run == "Y" or run == "y":
            return True
        elif run == "":
            if default_no:
                Msg.warn("I choose the negative option (N)!", log=False)
                return False
            else:
                Msg.warn("I choose the affirmative option (Y)!", log=False)
                return True
        else:
            print("Unknown input!")
            return Msg.dialog(default_no)

    @staticmethod
    def msg(msg: str, startl="", endl="\n", log: bool = True) -> None:
        print(f"{startl}\033[1m==>\033[0m {msg}", end=endl)
        if log:
            log_msg(f"{startl}{msg}{endl}")

    @staticmethod
    def err(msg: str, startl="", endl="\n", log: bool = True) -> None:
        print(f"{startl}[\033[1m\033[31m!\033[0m] {msg}", file=sys.stderr, end=endl)
        if log:
            log_msg(f"{startl}{msg}{endl}", status="fail")

    @staticmethod
    def warn(msg: str, startl="", endl="\n", log: bool = True) -> None:
        print(f"{startl}[\033[33mW\033[0m] {msg}", file=sys.stderr, end=endl)
        if log:
            log_msg(f"{startl}{msg}{endl}", status="warn")

    @staticmethod
    def ok(msg: str, startl="", endl="\n", log: bool = True) -> None:
        print(f"{startl}[\033[32m✓\033[0m] {msg}", end=endl)
        if log:
            log_msg(f"{startl}{msg}{endl}", status=" ok ")

    @staticmethod
    def header(msg: str, color: str = "\033[1m"):
        try:
            term_size = os.get_terminal_size()[0]
            msg = f" {msg} "  # В пробелах для того, чтобы выглядело нормально
            hdr_msg = msg.center(term_size, '=')
        except BrokenPipeError:
            hdr_msg = f"--== {msg} --=="

        print(f"{color}{hdr_msg}\033[0m")
        log_msg(f"{msg}\n", status="info")
