#!/usr/bin/python3
#
# exceptions.py - base exceptions for cport package manager
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

class PortError(Exception):

    def __init__(self, value: str = None):
        self.value = value

    def __str__(self):
        return self.value


class CSystemError(PortError):

    def __init__(self, value: str = None):
        self.value = value

    def __str__(self):
        return self.value


class PortNotFoundError(PortError):

    def __init__(self, value=None):
        self.value = value

    def __str__(self):
        return self.value


class PortBrokenError(PortError):

    def __init__(self, value: str = None):
        self.value = value

    def __str__(self):
        return self.value


class PortFilesBrokenError(PortError):

    def __init__(self, value=None):
        self.value = value

    def __str__(self):
        return self.value


class FilePortTomlError(PortFilesBrokenError):

    def __init__(self, value=None):
        self.value = value

    def __str__(self):
        return self.value


class FileFilesListError(PortFilesBrokenError):

    def __init__(self, value=None):
        self.value = value

    def __str__(self):
        return self.value
