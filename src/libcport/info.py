#!/usr/bin/python3
#
# info.py - get information about ports
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
import toml

import libcport.core as cc
import libcport.datatypes as cd
import libcport.exceptions as ce


class CPortInfo:

    """Get data from the 'port.toml' file of the selected port"""

    def __init__(self, port: cd.port):
        self.port: cd.port = port
        conf_file: str = f"{port.path}/port.toml"

        if not os.path.isfile(conf_file):
            raise ce.FilePortTomlError

        self.data: dict = toml.load(conf_file)

    def package(self) -> dict:
        """Return all data from the '[package]' section (port.toml file) + installed status (True or False)

        Returned value:

            {
                "name": str,
                "version": str,
                "description": str,
                "maintainer": str,
                "releases": list[str],
                "priority": str,
                "usage": float,
                "upgrade_mode": str,
                "build_time": float,
                "installed": bool
            }
        """

        db = cc.CDatabase(self.port)

        installed: bool = db.check()

        db.close()

        self.data['package']['installed'] = installed
        return self.data['package']

    def deps(self) -> dict:
        """Return all data from the '[deps]' section

        Return value:

            {
                "required": str | None,
                "recommend": str | None,
                "optional": str | None
            }
        """

        return self.data.get('deps')

    def port(self) -> dict:
        """Return all data from the '[port]' section

        Return value:

            {
                "url": str | None,
                "file": str | None,
                "md5": str | None,
                "sha256": str | None
            }
        """

        return self.data['port']


def version_compare(ver_first: str, ver_second: str) -> bool:
    """Function to compare two versions

        :arg ver_first - first software version;
        :arg ver_second - second software version;

    If they are equal, True is returned. Otherwise, there is a check that
    ver_first > ver_second. Accordingly, True or False is returned.

    NOTE: only works with versions that consist only of numbers: "INT.INT"
    """

    v1_n = ver_first.split('.')
    v2_n = ver_second.split('.')

    for v1, v2 in zip(v1_n, v2_n):
        if int(v1) == int(v2):
            continue
        return int(v1) > int(v2)

    return ver_first > ver_second
