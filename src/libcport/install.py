#!/usr/bin/python3
#
# install.py - module with functions for build, install and check ports
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import libcport.datatypes as cd
import libcport.constants as ccc
from libcport.core import dbg_msg

import os
import toml
import wget
import tarfile
import zipfile
import subprocess
import hashlib


class CInstall:

    def __init__(self, port: cd.port):
        self.port = port
        self.data = toml.load(f"{port.path}/port.toml")

        # Optional parameters in the '[port]' section
        # Type: str | None
        self.url = self.data['port'].get('url')
        self.md5 = self.data['port'].get('md5')
        self.sha256 = self.data['port'].get('sha256')
        self.fname = self.data['port'].get('file')

    def check_fname(self) -> bool:
        dbg_msg(f"[check_fname()]: URL: {self.url}")

        if self.url is None or self.url == "none":
            return False

        return True

    def get_fname(self) -> str:
        """Method for getting the name of the file that will be downloaded from
        the link from the 'port.url' parameter. The archive name is calculated
        from the data in this parameter."""

        if self.fname is not None:
            return self.fname

        return wget.detect_filename(self.url)

    def download(self) -> bool:
        if self.url is None or self.url == "none":
            return True

        file = f"{ccc.CACHE_DOWNLOADED_DIR}/{self.get_fname()}"

        if ccc.USE_CACHE == "yes" and os.path.exists(file):
            return True

        wget.download(self.url, ccc.CACHE_DOWNLOADED_DIR)
        print()

        return os.path.isfile(file)

    def cksum(self) -> bool:
        fname = f"{ccc.CACHE_DOWNLOADED_DIR}/{self.get_fname()}"

        dbg_msg(f"Checking the '{fname}' file...")
        dbg_msg(f"URL: {self.url}")

        if self.sha256 is not None:
            dbg_msg("Use the sha256")

            _hash = hashlib.sha256()
            tgt = self.sha256
        else:  # self.md5 exist; self.sha256 - doesn't exist
            dbg_msg("Use the md5")
            assert self.md5 is not None

            _hash = hashlib.md5()
            tgt = self.md5

        if not os.path.isfile(fname):
            raise FileNotFoundError

        with open(fname, 'rb') as f:
            # The file is read in blocks of 4 KB, since there may be a case when
            # the file is large, and its "full" opening may either be incorrect
            # or fill up a fairly large amount of memory, which is not
            # recommended.
            for blk in iter(lambda: f.read(4096), b""):
                _hash.update(blk)

        return _hash.hexdigest() == tgt

    def unpack(self) -> None:
        """Supported archives formats:
         * tar;
         * zip.
        """

        file = f"{ccc.CACHE_DOWNLOADED_DIR}/{self.get_fname()}"

        dbg_msg(f"Unpacking file '{file}'...")

        # Проверка типа файла
        if zipfile.is_zipfile(file):
            dbg_msg("This is zip file")
            with zipfile.ZipFile(file, mode='r') as f:
                f.extractall(ccc.CACHE_UNPACKED_DIR)
        else:
            dbg_msg("This is tar file")
            with tarfile.open(file, 'r') as f:
                f.extractall(path=ccc.CACHE_UNPACKED_DIR)

    def install(self, command: str = "") -> int:
        build_script = f"{self.port.path}/install"

        dbg_msg(f"Build script: '{build_script}'")

        run = subprocess.run(f"{command} {build_script}", shell=True)
        return run.returncode
