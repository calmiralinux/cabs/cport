#!/usr/bin/python3
#
# remove.py - module with functions for remove ports
# Copyright (C) 2021, 2022 Michail Krasnov <linuxoid85@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import libcport.datatypes as cd
from libcport.core import dbg_msg

import os
import toml
import shutil
import subprocess


class CRemove:

    def __init__(self, port: cd.port):
        self.port = port
        self.port_conf = toml.load(f"{self.port.path}/port.toml")

    def check_priority(self) -> bool:
        """Returned True if port has a 'system' priority
        or False if port has a 'user' priority"""

        return self.port_conf['package']['priority'] == 'system'

    def files(self) -> list[str]:
        fls = f"{self.port.path}/files.list"
        with open(fls) as f:
            data = f.read().splitlines()

        for file in data:
            dbg_msg(f"File '{file}:", endl=" ")

            if os.path.exists(file):
                dbg_msg("found")
            else:
                dbg_msg("\033[31mNOT found\033[0m")
        return data

    def run_preremove(self) -> int:
        prerem = f"{self.port.path}/preremove.sh"
        if os.path.isfile(prerem):
            dbg_msg(f"Run pre-remove script of port '{self.port.name}'")
            run = subprocess.run(prerem, shell=True)
            return run.returncode
        return 0

    def run_postremove(self) -> int:
        postrem = f"{self.port.path}/postremove.sh"
        if os.path.isfile(postrem):
            dbg_msg(f"Run post-remove script of port '{self.port.name}'")
            run = subprocess.run(postrem, shell=True)
            return run.returncode
        return 0

    def remove(self) -> None:
        for file in self.files():
            if os.path.isfile(file):
                dbg_msg(f"Removing file '{file}'")
                os.remove(file)
            elif os.path.isdir(file):
                dbg_msg(f"Removing directory '{file}'")
                shutil.rmtree(file)
            elif os.path.islink(file):
                dbg_msg(f"Removing link '{file}'")
                os.remove(file)
