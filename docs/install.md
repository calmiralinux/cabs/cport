# Установка портов

Одна из самых важных функций, которую в значительной степени упрощает и
автоматизирует cport - это сборка пакета из исходного кода и установка его в
систему. Для корректной сборки пакета иногда приходится вводить достаточно
большое число команд в терминал, и далеко не все из этих команд короткие и/или
простые. cport же это всё упрощает тем, что все необходимые для сборки действия
записаны в BASH-скрипт, являющийся инструкцией по сборке. При желании или при
необходимости пользователь может отредактировать эти сборочные инструкции в
зависимости от целей и запросов.

Для установки порта используется ключ `--install` (сокращённая версия: `-i`).
Далее этому ключу указывается порт или несколько портов, которые необходимо
собрать.

Сначала менеджер системы портов выведет подробную информацию о программном
обеспечении, которое хочет собрать и установить пользователь, а потом запросит
подтверждение действий (нужно ввести `y` или `n` и нажать `Enter` для
продолжения или прекращения действий соотв.). В том случае, если пользователь
ответил утвердительно, начнётся скачивание архива с исходным кодом порта, его
распаковка (на данный момент поддерживаются только архивы tar) и выполнение
сборочных инструкций.

Например:

```bash
cport -i xorg/gtk3 editors/gvim
```

Эта команда установит порты `xorg/gtk3` и `editors/gvim`.

Подтверждение вызывается для каждого действия. Т.е. если вы, например, указали
сборку одного порта, то у пользователя будет запрошено лишь одно подтверждение
действий. Если пользователь указал сборку десяти портов, то запрос о продолжении
действий будет задан десять раз.

Если пользователь по каким-то причинам не сможет ответить на запросы, то он
может отключить их, указав ключ `--yes` (сокращённая версия: `-y`). В таком
случае запрос о продолжении действий не будет задан ни разу. Считайте, что
`cport` будет автоматически отвечать утвердительно на все запросы.

Например:

```
cport -i editors/gvim
```

![](pic/cport1.png)

Здесь мы видим, что после того, как была выведена информация о порте, cport
запрашивает пользователя, продолжить ли ему работу или нет. В том случае, если
пользователь введёт `y` и нажмёт `Enter`, то cport скачает архив с исходным
кодом порта, распакует его и приступит к сборке.

```
cport --yes -i editors/gvim
```

![](pic/cport2.png)

Во втором случае мы указали ключ `--yes` (или его сокращённую версию `-y`).
Тогда cport не будет ничего запрашивать у пользователя и сразу присупит к
сборке.
