DESTDIR=/
PYVERSION="3.10"

binary:
	python3 -m nuitka --low-memory --onefile src/cport.py

install-binary:
	mkdir -p ${DESTDIR}usr/bin
	mkdir -p ${DESTDIR}usr/share/cport
	mkdir -p ${DESTDIR}var/lib/Calmira
	mkdir -p ${DESTDIR}var/cache/cport/downloads
	cp -v cport.bin ${DESTDIR}usr/bin/cport
	cp -v src/cport_cli/shared.sh ${DESTDIR}usr/share/cport

remove-binary:
	rm -v ${DESTDIR}usr/bin/cport

clean:
	rm -rf cport.{build,dist,onefile-build} cport.bin

install:
	mkdir -pv /usr/lib/python${PYVERSION}/site-packages
	mkdir -p ${DESTDIR}usr/lib/python${PYVERSION}/site-packages
	mkdir -p ${DESTDIR}usr/bin
	mkdir -p ${DESTDIR}var/lib/Calmira
	mkdir -p ${DESTDIR}usr/share/cport
	mkdir -p ${DESTDIR}var/cache/cport/downloads
	cp -rv src/{cport_cli,libcport} ${DESTDIR}usr/lib/python${PYVERSION}/site-packages/
	cp -rv src/cport_cli/shared.sh ${DESTDIR}usr/share/cport
	cp -v src/cport.py ${DESTDIR}usr/bin/cport

remove:
	rm -rvf ${DESTDIR}/usr/{bin/cport,lib/python${PYVERSION}/site-packages/{cport_cli,libcport}}
	rm -rvf ${DESTDIR}/var/{cache/cport,lib/Calmira}
