from distutils.core import setup

setup(
    name='cport',
    version='v1.0b2',
    packages=['libcport', 'cport_cli'],
    package_dir={'libcport': 'src/libcport', 'cport_cli': 'src/cport_cli'},
    url='https://gitlab.com/calmiralinux/cabs/cport',
    license='GNU GPLv3',
    author='cov_id111',
    author_email='michail383krasnov@mail.ru',
    description='cport - менеджер системы портов дистрибутива Calmira GNU/Linux'
)
